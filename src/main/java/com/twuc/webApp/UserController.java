package com.twuc.webApp;

import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.ArrayList;

@RestController
@RequestMapping("/api")
public class UserController {

    @GetMapping("/bind/int/{value}")
    public String testBindToIn(@PathVariable int value){
        return "Successful";
    }

    @GetMapping("/bind/{value}")
    public Integer testBindToInteger(@PathVariable Integer value){
        return value;
    }

    @GetMapping("/users/{userId}/books/{bookId} ")
    public String testBindToIntegerAndInt(@PathVariable Integer userId, @PathVariable int bookId){
        return "Successful";
    }

    @GetMapping("/getuser")
    public String getInfoByUserName(String username){
        return username;
    }

    @GetMapping("/getuserbydeafult")
    public String getInfoByDeafult(@RequestParam(defaultValue = "xiaoming") String username){
        return username;
    }

    @GetMapping("/bind/collection")
    public ArrayList<String> getCollection(@RequestParam ArrayList<String> myparam){
        System.out.println(myparam);
        return myparam;
    }

    @PostMapping("/datetimes")
    public LocalDate getDate(@RequestBody MyDate myDate){
        return myDate.getDateTime();
    }


    @PostMapping("/users/notnull")
    public String getUserName(@RequestBody @Valid User user) {
        return user.getName();
    }

    @PostMapping("/size")
    public String getUserNameWithSize(@RequestBody @Valid User user) {
        return user.getName();
    }

    @PostMapping(value = "/minAndMax")
    public String getInfoByNameYear(@RequestBody @Valid User user) {
        return String.format("%s birth is %d", user.getName(), user.getYearOfBirth());
    }

    @PostMapping(value = "/email")
    public String getUserEmail(@RequestBody @Valid User user) {
        return String.format("%s birth is %d,and his email %s", user.getName(), user.getYearOfBirth(), user.getEmail());
    }
}
