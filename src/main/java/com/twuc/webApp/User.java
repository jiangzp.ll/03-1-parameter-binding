package com.twuc.webApp;

import javax.validation.constraints.*;

public class User {

    @NotNull
    @Size(min = 2, max = 10)
    private String name;

    @Min(1000) @Max(9999)
    private Integer yearOfBirth;

    @Email
    private String email;

    public User(@NotNull @Size(min = 2, max = 10)String name, @Min(1000) @Max(9999) Integer yearOfBirth) {
        this.name = name;
        this.yearOfBirth = yearOfBirth;
    }

    public User(@NotNull @Size(min = 2, max = 10)String name, @Min(1000) @Max(9999) Integer yearOfBirth, String email) {
        this.name = name;
        this.yearOfBirth = yearOfBirth;
        this.email = email;
    }

    public User() {
    }

    public User(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public Integer getYearOfBirth() {
        return yearOfBirth;
    }

    public String getEmail() {
        return email;
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                '}';
    }
}
