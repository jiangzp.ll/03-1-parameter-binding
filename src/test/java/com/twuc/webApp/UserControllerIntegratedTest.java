package com.twuc.webApp;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;
@SpringBootTest
@AutoConfigureMockMvc
class UserControllerIntegratedTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void should_return_200_when_bind_to_int() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/bind/int/15"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("Successful"));
    }


    @Test
    void should_return_200_when_bind_to_integer() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/bind/15"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("15"));
    }


    @Test
    void should_return_200_when_bind_to_int_and_integer() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/users/15/books/15 "))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("Successful"));
    }

    @Test
    void should_return_200_when_bind_query_string() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/getuser?username=xiaoming"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("xiaoming"));
    }

    @Test
    void should_return_200_and_query_when_bind_query_string() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/getuser?username=xiaoming"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("xiaoming"));
    }

    @Test
    void should_return_200_and_query_user_deafult_query_string() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/getuserbydeafult"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("xiaoming"));
    }

    @Test
    void should_bind_collection_to_param() throws Exception {
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("myValue1");
        arrayList.add("myValue2");
        arrayList.add("myValue3");
        mockMvc.perform(MockMvcRequestBuilders.get("/api/bind/collection?myparam=myValue1&myparam=myValue2&myparam=myValue3"))
                .andExpect(MockMvcResultMatchers.status().is(200));
    }

    @Test
    void should_return_date_when_query_date_string() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/datetimes").content("{ \"dateTime\": \"2019-10-01T10:00:00Z\" }")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("\"2019-10-01\""));
    }


    @Test
    void should_return_date_when_query_incorrect_date_string() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/datetimes").content("{ \"dateTime\": \"2019-10-01\" }")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("\"2019-10-01\""));
    }

    //2.4
    //2.1@NotNull
    @Test
    void should_volida_NotNull_of_not_successful_400() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/users/notnull")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{name}"))
                .andExpect(MockMvcResultMatchers.status().is(400));
    }

    @Test
    void should_volida_NotNull_of_successful_200() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/users/notnull")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\"name\":\"xiaoming\"}"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("xiaoming"));
    }

    //2.2 @Size

    @Test
    void should_volida_Size_is_between_min_and_max() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/size")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\"name\":\"xiaoming\"}"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("xiaoming"));
    }

    @Test
    void should_volida_Size_is_not_between_min_and_max() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/size")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\"name\":\"a\"}"))
                .andExpect(MockMvcResultMatchers.status().is(400));
    }
    //2.3@Min @Max
    @Test
    void should_volida_user_birth_is_between_min_and_max() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/minAndMax")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\"name\":\"xiaoming\",\"yearOfBirth\":\"4000\"}"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("xiaoming birth is 4000"));
    }

    @Test
    void should_volida_user_birth_is_not_between_min_and_max() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/minAndMax")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\"name\":\"xiaoming\",\"yearOfBirth\":\"18\"}"))
                .andExpect(MockMvcResultMatchers.status().is(400));
    }

    //2.4@Email
    @Test
    void should_volida_user_emaill_is_right() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/email")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\"name\":\"xiaoming\",\"yearOfBirth\":\"4000\",\"email\":\"123@thoughtworks.com\"}"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("xiaoming birth is 4000,and his email 123@thoughtworks.com"));
    }

    @Test
    void should_volida_user_emaill_is_wrong() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/email")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\"name\":\"xiaoming\",\"yearOfBirth\":\"4000\",\"email\":\"123thoughtworks.com\"}"))
                .andExpect(MockMvcResultMatchers.status().is(400));
    }

}